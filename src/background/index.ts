globalThis.window = {
  location: {
    reload: () => chrome.runtime.reload(),
  },
} as typeof window;

// @ts-expect-error loadScript
__webpack_require__.l = async function (url, done) {
  const code = await fetch(url).then(res => res.text());

  const hasUpdate = /^\/\*\*\*\/ ".+":$/m.test(code);
  if (hasUpdate) {
    chrome.runtime.reload();
  }

  const [, hash] = /__webpack_require__\.h =.+?"(\w+)"/.exec(code) || [];
  // @ts-expect-error
  self.webpackHotUpdatewebx('background', {}, __webpack_require__ => {
    __webpack_require__.h = () => hash;
  });

  done(null);
};

console.log('Hello, world');
