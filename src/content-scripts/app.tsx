import { useState } from 'react';
import logo from '@/assets/logo.png';
import './styles.css';

export const App = () => {
  const [count, setCount] = useState(0);
  return (
    <div>
      <img src={logo} width={200} alt="Logo" />
      <div className="title">WebX From Scratch</div>
      <button onClick={() => setCount(count + 1)}>Count: {count}</button>
    </div>
  );
};
