import { createRoot } from 'react-dom/client';
import { App } from './app';

// @ts-expect-error
__webpack_require__.l = async function (url, done) {
  await import(/* webpackIgnore: true */ url);
  done(null);
};

const root = document.createElement('div');
const shadowRoot = root.attachShadow({ mode: 'open' });
document.body.append(root);

fetch(chrome.runtime.getURL('static/css/content-scripts.css'))
  .then(response => response.text())
  .then(cssContent => new CSSStyleSheet().replace(cssContent))
  .then(styleSheet => shadowRoot.adoptedStyleSheets.push(styleSheet));

createRoot(shadowRoot).render(<App />);
