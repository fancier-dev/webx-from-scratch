const isDev = process.env.NODE_ENV === 'development';

const manifest: chrome.runtime.ManifestV3 = {
  manifest_version: 3,
  name: 'WebX From Scratch',
  version: '0.0.0',
  background: {
    service_worker: 'static/js/background.js',
  },
  action: {
    default_popup: 'html/popup/index.html',
  },
  options_ui: {
    page: 'html/options/index.html',
  },
  content_scripts: [
    {
      matches: ['<all_urls>'],
      js: ['static/js/content-scripts.js'],
    },
  ],
  web_accessible_resources: [
    {
      matches: ['<all_urls>'],
      resources: ['static/css/*', 'static/image/*'],
    },
  ],
  ...(isDev
    ? {
        content_security_policy: {
          extension_pages:
            "script-src 'self' http://localhost:8080/; object-src 'self' http://localhost:8080/",
        },
      }
    : {}),
};

export default manifest;
