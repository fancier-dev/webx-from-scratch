import { appTools, defineConfig } from '@modern-js/app-tools';
import createJITI from 'jiti';

// https://modernjs.dev/en/configure/app/usage
export default defineConfig({
  runtime: {
    router: true,
  },
  plugins: [appTools()],
  source: {
    entriesDir: './src/pages',
  },
  dev: {
    assetPrefix: true,
  },
  output: {
    copy: [
      {
        from: './src/manifest.ts',
        to: './manifest.json',
        // @ts-expect-error
        transform(_content, filepath) {
          const jiti = createJITI(filepath, {
            requireCache: false,
            interopDefault: true,
          });
          const manifest = jiti(filepath);
          return JSON.stringify(manifest, null, 2);
        },
      },
    ],
    disableInlineRuntimeChunk: true,
    disableFilenameHash: true,
    disableMinimize: true,
    polyfill: 'off',
    overrideBrowserslist: ['last 2 chrome versions'],
  },
  tools: {
    webpackChain(chain, { isProd }) {
      chain.entry('content-scripts').add('src/content-scripts/index.tsx');
      if (isProd) {
        chain
          .entry('content-scripts')
          .prepend(
            `data:text/javascript,__webpack_require__.p = chrome.runtime.getURL('/');`,
          );
      }
      chain.entry('background').add('src/background/index.ts');

      const allInOneEntries = new Set(['background', 'content-scripts']);
      chain.optimization.runtimeChunk(false).splitChunks({
        chunks: chunk => {
          return !allInOneEntries.has(chunk.getEntryOptions()?.name || '');
        },
      });
    },
    devServer: {
      client: {
        protocol: 'ws',
        host: 'localhost',
      },
    },
  },
});
